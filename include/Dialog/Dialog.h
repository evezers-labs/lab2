//   This is a personal academic project. Dear PVS-Studio, please check it.
//
//   PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <iostream>

#ifndef CARDIOID_DIALOG_H
#define CARDIOID_DIALOG_H

#include "../Cardioid/Cardioid.h"

namespace DialogLib{
    struct DialogItem{
        const char *name;
        CardioidLib::Cardioid *(*functionPtr)(CardioidLib::Cardioid *c);
    };

    class Dialog {
    private:
        const DialogItem *pItems;
        const unsigned int itemCount;

        CardioidLib::Cardioid cardioid;
    public:
        Dialog(const DialogItem *pItems, const unsigned int itemCount):pItems(pItems), itemCount(itemCount){}

        void PrintMenu() const;
        [[nodiscard]] CardioidLib::Cardioid *(*GetUserSelection() const)(CardioidLib::Cardioid *c);
        int Start();

        static CardioidLib::Cardioid *GetParams(CardioidLib::Cardioid *c);
        static CardioidLib::Cardioid *SetParams(CardioidLib::Cardioid *c);
        static CardioidLib::Cardioid *CenterDistance(CardioidLib::Cardioid *c);
        static CardioidLib::Cardioid *MaxAxisDistanceCoords(CardioidLib::Cardioid *c);
        static CardioidLib::Cardioid *CurvatureOfMainPoints(CardioidLib::Cardioid *c);
        static CardioidLib::Cardioid *Square(CardioidLib::Cardioid *c);
        static CardioidLib::Cardioid *ArcLength(CardioidLib::Cardioid *c);
        static CardioidLib::Cardioid *Exit(CardioidLib::Cardioid *c);
    };
}

#endif //CARDIOID_DIALOG_H
