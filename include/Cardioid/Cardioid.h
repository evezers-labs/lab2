//   This is a personal academic project. Dear PVS-Studio, please check it.
//
//   PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#ifndef CYCLOID_CARDIOID_H
#define CYCLOID_CARDIOID_H

#include <stdexcept>

namespace CardioidLib{
    enum DirectionEnum {LEFT, RIGHT, UP, DOWN};

    struct CoordinatePair{
        double x1, y1,
                x2, y2;
    };

    struct MainPointsCurvature{
        double CuspCurvature,
               TopCurvature;
    };

    struct RevolutionAngle{
        unsigned int RevolutionCount;
        double Angle;
    };

    RevolutionAngle ConvertAngle(double angle);


    class Cardioid{
    private:
        double CircleRadius;
        DirectionEnum direction;

        [[nodiscard]] double BaseArcLength(double angle) const;
    public:
        explicit Cardioid(double CircleRadius = 0, DirectionEnum direction = LEFT):direction(direction){
            if (CircleRadius < 0) throw std::runtime_error("Radius must be positive");
            this->CircleRadius = CircleRadius;
        }

        double SetBaseRadius(double NewCircleRadius) {
            if (NewCircleRadius < 0) throw std::runtime_error("Radius must be positive");
            CircleRadius = NewCircleRadius;

            return NewCircleRadius;
        };

        [[nodiscard]] double GetBaseRadius() const { return CircleRadius; }

        DirectionEnum SetDirection(DirectionEnum NewDirection) { direction = NewDirection; return NewDirection; }

        [[nodiscard]] DirectionEnum GetDirection() const { return direction; }

        [[nodiscard]] double CenterDistance(double angle) const;
        [[nodiscard]] CoordinatePair MaxAxisDistanceCoords() const;
        [[nodiscard]] MainPointsCurvature CurvatureOfMainPoints() const {
            return MainPointsCurvature{0, 8 * CircleRadius / 3};}
        [[nodiscard]] double Square() const { return 6 * std::numbers::pi * CircleRadius * CircleRadius; }
        [[nodiscard]] double ArcLength(double angle) const;
    };
}

#endif //CYCLOID_CARDIOID_H