# lab2

[![PVS-Studio analyze](https://user-content.gitlab-static.net/97589024b840e6fd1684fb18ab87a17619b8a8ef/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f5056532d2d53747564696f2053746174696320416e616c797369732d736565207265706f72742d626c7565)](https://gitlab.com/evezers-labs/lab2/-/jobs/artifacts/main/file/PVS-Studio/full-report/index.html?job=pvs-studio-analyzer)
[![pipeline status](https://gitlab.com/evezers-labs/lab2/badges/main/pipeline.svg)](https://gitlab.com/evezers-labs/lab2/-/pipelines/latest)
[![coverage report](https://gitlab.com/evezers-labs/lab2/badges/main/coverage.svg)](https://gitlab.com/evezers-labs/lab2/-/jobs/artifacts/main/file/bin/cmake-build-debug-coverage/coverage-report/report.html?job=test)

Build project:
```bash
cmake -B bin/cmake-build-debug/
cd bin/cmake-build-debug/
make
```

Tests:
```bash
cmake -B bin/cmake-build-debug-coverage/ -DCMAKE_CXX_FLAGS=--coverage -DCMAKE_C_FLAGS=--coverage
cd bin/cmake-build-debug-coverage/
make
make test
cd src
cat ../../../test/test1.in | ./lab2
cd ..
mkdir coverage-report
gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage-report/coverage.xml --root ../../../ --html coverage-report/report.html --html-details
```

Valgrind:

```bash
valgrind bin/cmake-build-debug/test/lab2_test
```


### Требования

1. Проект на git'е. (обязательно)

2. Разработка через тестирование (TDD), полное покрытие методов класса модульными тестами (обязательно)

3. Иcпользование gtest или аналогов (для VS2019 тип проекта Google Test (рекомендуется) либо внешния библиотека тестирования, возможно использовать Native Unit Test Project) (обязательно)

4. Корректно спроектированный класс, отсутсвие избыточности в состоянии, корректная сигнатура методов, обработка ошибочных ситуаций и т.п. (обязательно)

5. Структура решения: проект со статически линкуемой библиотекой с классом, проект консольного приложения для диалоговой отладки, проект для модульного тестирования. (обязательно)

6. Статический анализ кода, встроенный инструментарий в IDE (пр. VS2019: Analyze->Run Code Analysis, см.  также Project -> Properties -> Configuration Properties -> Code Analysis -> Microsoft -> Active Rules) или внешние инструменты (Sonarqube + extensions, Clang Static Analyzer и д.р.) (обязательно знакомство с инструментом, все исправлять не надо, т.к. некоторые вещи просто нельзя исправить без использования STL)

7. Динамический анализ на утечки памяти, встроенный инструментарий в IDE / библиотеки (Пр., VS2019) или внешние инструменты (valgrind, Deleaker и т.п.). (обязательно)

8. Стандарт языка C++17 (рекомендуется), С++20 (при наличии). Допустим C++11 или С++14 (если почему-то нет С++17)

9. Проверка корректности ввода (работа через потоки C++) (обязательно)

10. Не "кривой", не избыточный, поддерживаемый и расширяемый код (разумная декомпозиция, DRY, корректное использование заголовочных файлов и т.п.) (обязательно)

11. Использование контейнеров из STL возможно только для "профессионалов" в С++, подавляющему большинству не рекомендуется (дождитесь задачи №4).


## Вариант 7. Кардиоида

[![desmos plot](https://img.shields.io/badge/Cardioid-Desmos-brightgreen)](https://www.desmos.com/calculator/lbs3l4wgzt)

![epicycloid k=1](docs/EpicycloidK1.gif)

Разработать класс, определяющий кривую – кардиоиду.

1. Определить состояние класса.

2. Разработать необходимые конструкторы и методы получения и изменения параметров, определяющих кривую.

3. Вернуть расстояние до центра в полярной системе координат в зависимости от угла для точки принадлежащей кардиоиде.

4. Вернуть координаты наиболее удаленных от оси кардиоиды точек.

5. Вернуть радиуса кривизны в характерных точках кардиоиды.

6. Вернуть площадь описываемую кардиоидой.

7. Вернуть длину дуги кардиоиды в зависимости от угла полярного радиуса.

Разработать диалоговую программу для тестирования класса.




