//   This is a personal academic project. Dear PVS-Studio, please check it.
//
//   PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <cmath>

#include "../../include/Cardioid/Cardioid.h"

namespace CardioidLib{
    RevolutionAngle ConvertAngle(double angle){
        RevolutionAngle new_angle {0, 0};

        if (angle < 0) angle = -angle;

        new_angle.RevolutionCount = (unsigned int) floor(angle /  (2 * std::numbers::pi));
        new_angle.Angle = angle - new_angle.RevolutionCount * 2 * std::numbers::pi;

        return new_angle;
    }

    double Cardioid::CenterDistance(double angle) const{
        if (direction == LEFT){
            return 2 * CircleRadius * (1 - cos(angle));
        } else if (direction == RIGHT){
            return 2 * CircleRadius * (1 + cos(angle));
        } else if (direction == UP){
            return 2 * CircleRadius * (1 + sin(angle));
        } else if (direction == DOWN){
            return 2 * CircleRadius * (1 - sin(angle));
        }

        return 0;
    }

    CoordinatePair Cardioid::MaxAxisDistanceCoords() const{
        CoordinatePair coord {0, 0, 0, 0};

        if (direction == LEFT){
            coord.x1 = -3.0/2 * CircleRadius;
            coord.x2 = -3.0/2 * CircleRadius;
            coord.y1 = 3 * sqrt(3) / 2 * CircleRadius;
            coord.y2 = -3 * sqrt(3) / 2 * CircleRadius;
        } else if (direction == RIGHT){
            coord.x1 = 3.0/2 * CircleRadius;
            coord.x2 = 3.0/2 * CircleRadius;
            coord.y1 = 3 * sqrt(3) / 2 * CircleRadius;
            coord.y2 = -3 * sqrt(3) / 2 * CircleRadius;
        } else if (direction == UP){
            coord.x1 = 3 * sqrt(3) / 2 * CircleRadius;
            coord.x2 = -3 * sqrt(3) / 2 * CircleRadius;
            coord.y1 = 3.0/2 * CircleRadius;
            coord.y2 = 3.0/2 * CircleRadius;
        } else if (direction == DOWN){
            coord.x1 = 3 * sqrt(3) / 2 * CircleRadius;
            coord.x2 = -3 * sqrt(3) / 2 * CircleRadius;
            coord.y1 = -3.0/2 * CircleRadius;
            coord.y2 = -3.0/2 * CircleRadius;
        }
        
        return coord;
    }

    double Cardioid::BaseArcLength(double angle) const{
        return 8 * CircleRadius * (1 - cos(angle / 2));
    }

    double Cardioid::ArcLength(double angle) const{
        double deltaAngle = 0;

        if (direction == LEFT){
            deltaAngle = 0;
        } else if (direction == RIGHT){
            deltaAngle = std::numbers::pi;
        } else if (direction == UP){
            if (angle > 0)
                deltaAngle = std::numbers::pi / 2;
            else
                deltaAngle = 3 * std::numbers::pi / 2;
        } else if (direction == DOWN){
            if (angle > 0)
                deltaAngle = 3 * std::numbers::pi / 2;
            else
                deltaAngle = std::numbers::pi / 2;
        }

        double currentCycloidLength;

        double startAngle, endAngle;

        RevolutionAngle revolutionAngle = ConvertAngle(angle);

        double baseArcAngle = deltaAngle + revolutionAngle.Angle;

        if (baseArcAngle > 2 * std::numbers::pi){
            startAngle = ConvertAngle(baseArcAngle).Angle;
            endAngle = deltaAngle;

            currentCycloidLength = 16 * CircleRadius - (BaseArcLength(endAngle) - BaseArcLength(startAngle));
        } else {
            startAngle = deltaAngle;
            endAngle = baseArcAngle;

            currentCycloidLength = BaseArcLength(endAngle) - BaseArcLength(startAngle);
        }

        double FullCycloidLengths = 16 * CircleRadius * revolutionAngle.RevolutionCount;

        return FullCycloidLengths + currentCycloidLength;
    }

}