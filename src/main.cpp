//   This is a personal academic project. Dear PVS-Studio, please check it.
//
//   PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "../include/Dialog/Dialog.h"

int main() {
    DialogLib::DialogItem items[] {
        "Exit", DialogLib::Dialog::Exit,
        "Get params", DialogLib::Dialog::GetParams,
        "Set params", DialogLib::Dialog::SetParams,
        "Center distance", DialogLib::Dialog::CenterDistance,
        "Max axis distance coordinates", DialogLib::Dialog::MaxAxisDistanceCoords,
        "Curvature of main points", DialogLib::Dialog::CurvatureOfMainPoints,
        "Arc length", DialogLib::Dialog::ArcLength,
        "Square", DialogLib::Dialog::Square
    };

    DialogLib::Dialog dialog(items, sizeof(items) / sizeof(items[0]));

    return dialog.Start();
}