//   This is a personal academic project. Dear PVS-Studio, please check it.
//
//   PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "../../include/Dialog/Dialog.h"

namespace DialogLib{
    template <typename T>
    void getNum(T &num){
        do {
            std::cin >> num;
            if (std::cin.fail()){
                std::cin.clear();
                std::cin.ignore(32767,'\n');

                std::cerr << std::endl <<  "Incorrect input format!" << std::endl;
            } else {
                std::cin.ignore(32767,'\n');

                return;
            }

            if (std::cin.eof()) throw std::runtime_error("Unexpected input end");
        } while (true);
    }

    void Dialog::PrintMenu() const {
        std::cout << std::endl;
        std::cout << "MAIN MENU: " << std::endl;

        for (int i = 0; i < itemCount; ++i){
            std::cout << i << ". " << pItems[i].name << std::endl;
        }
    }

    CardioidLib::Cardioid *(*Dialog::GetUserSelection() const)(CardioidLib::Cardioid *c) {
        unsigned int num;

        do {
            std::cout << "Enter menu entry number: ";
            getNum(num);
            std::cout << std::endl;

            if (num > itemCount - 1) std::cerr << "Enter value from 0 to " << itemCount - 1 << std::endl;
        } while (num > itemCount - 1);

        return pItems[num].functionPtr;
    }

    int Dialog::Start() {
        try {
            do {
                PrintMenu();
            } while (GetUserSelection()(&cardioid) != nullptr);
        } catch (const std::runtime_error &runtimeError){
            std::cerr << runtimeError.what() << std::endl;

            Exit(&cardioid);

            return 1;
        }

        return 0;
    }

    CardioidLib::Cardioid *Dialog::GetParams(CardioidLib::Cardioid *c){
        std::cout << "Current parameters: "
                  << "base radius = " << c->GetBaseRadius()
                  << ", direction = " << c->GetDirection()
                  << std::endl;

        return c;
    }

    CardioidLib::Cardioid *Dialog::SetParams(CardioidLib::Cardioid *c){
        double radius;
        unsigned int directionID;
        CardioidLib::DirectionEnum direction;

        do {
            std::cout << "Enter base circle radius: ";
            getNum(radius);
            std::cout << std::endl;

            if (radius < 0) std::cerr << "Radius must be positive" << std::endl;
        } while (radius < 0);

        do {
            std::cout << "Enter direction (0 - LEFT, 1 - RIGHT, 2 - UP, 3 - DOWN): ";
            getNum(directionID);
            std::cout << std::endl;

            if (directionID > 3) std::cerr << "Enter value from 0 to 3" << std::endl;
        } while (directionID > 3);

        direction = (CardioidLib::DirectionEnum) directionID;

        c->SetBaseRadius(radius);
        c->SetDirection(direction);

        std::cout << "Successfully set parameters: "
            << "base radius = " << c->GetBaseRadius()
            << ", direction = " << c->GetDirection()
            << std::endl;

        return c;
    }

    CardioidLib::Cardioid *Dialog::CenterDistance(CardioidLib::Cardioid *c){
        double angle;

        std::cout << "Enter angle: ";

        getNum(angle);

        std::cout << "Center distance is " << c->CenterDistance(angle) << std::endl;

        return c;
    }

    CardioidLib::Cardioid *Dialog::MaxAxisDistanceCoords(CardioidLib::Cardioid *c){
        CardioidLib::CoordinatePair coordinatePair = c->MaxAxisDistanceCoords();

        std::cout << "Max axis distance coordinates: " << std::endl
            << "x1: " << coordinatePair.x1 << std::endl
            << "y1: " << coordinatePair.y1 << std::endl
            << "x2: " << coordinatePair.x2 << std::endl
            << "y2: " << coordinatePair.y2 << std::endl;

        return c;
    }

    CardioidLib::Cardioid *Dialog::CurvatureOfMainPoints(CardioidLib::Cardioid *c){
        CardioidLib::MainPointsCurvature mainPointsCurvature = c->CurvatureOfMainPoints();

        std::cout << "Max axis distance coordinates: " << std::endl
                  << "Top curvature: " << mainPointsCurvature.TopCurvature << std::endl
                  << "Cusp curvature: " << mainPointsCurvature.CuspCurvature << std::endl;

        return c;
    }

    CardioidLib::Cardioid *Dialog::Square(CardioidLib::Cardioid *c){
        std::cout << "Square of cardioid is " << c->Square() << std::endl;

        return c;
    }

    CardioidLib::Cardioid *Dialog::ArcLength(CardioidLib::Cardioid *c){
        double angle;

        std::cout << "Enter angle: ";

        getNum(angle);

        std::cout << "Arc length is " << c->ArcLength(angle) << std::endl;

        return c;
    }

    CardioidLib::Cardioid *Dialog::Exit(CardioidLib::Cardioid *){
        std::cout << "Good bye!\n";

        return nullptr;
    }
}
