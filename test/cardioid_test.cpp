//   This is a personal academic project. Dear PVS-Studio, please check it.
//
//   PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include <gtest/gtest.h>
#include <cmath>
#include "../include/Cardioid/Cardioid.h"

using namespace CardioidLib;

TEST(CycloidTest, InitializationTests){
    auto *c = new Cardioid;

    EXPECT_EQ(c->GetBaseRadius(), 0);
    EXPECT_EQ(c->GetDirection(), LEFT);

    delete c;

    c = new Cardioid(2);

    EXPECT_EQ(c->GetBaseRadius(), 2);
    EXPECT_EQ(c->GetDirection(), LEFT);

    delete c;
    
    c = nullptr;

    EXPECT_THROW(c = new Cardioid(-2), std::runtime_error);

    delete c;

    c = new Cardioid(5, UP);

    EXPECT_EQ(c->GetBaseRadius(), 5);
    EXPECT_EQ(c->GetDirection(), UP);

    delete c;

    auto *c_array = new Cardioid[3]{Cardioid(6), Cardioid(2, RIGHT),  Cardioid(7, DOWN)};

    EXPECT_EQ(c_array[0].GetBaseRadius(), 6);
    EXPECT_EQ(c_array[0].GetDirection(), LEFT);

    EXPECT_EQ(c_array[1].GetBaseRadius(), 2);
    EXPECT_EQ(c_array[1].GetDirection(), RIGHT);

    EXPECT_EQ(c_array[2].GetBaseRadius(), 7);
    EXPECT_EQ(c_array[2].GetDirection(), DOWN);

    delete[] c_array;

    Cardioid c1, c2;

    c1.SetBaseRadius(6);
    c1.SetDirection(RIGHT);

    c2 = c1;

    EXPECT_EQ(c2.GetBaseRadius(), 6);
    EXPECT_EQ(c2.GetDirection(), RIGHT);
}

TEST(CycloidTest, SetterTests){
    auto *c = new Cardioid;

    EXPECT_EQ(c->GetBaseRadius(), 0);
    EXPECT_EQ(c->GetDirection(), LEFT);

    c->SetBaseRadius(8);

    EXPECT_EQ(c->GetBaseRadius(), 8);
    EXPECT_EQ(c->GetDirection(), LEFT);

    c->SetDirection(DOWN);

    EXPECT_EQ(c->GetBaseRadius(), 8);
    EXPECT_EQ(c->GetDirection(), DOWN);

    delete c;

    c = new Cardioid;

    EXPECT_THROW(c->SetBaseRadius(-2), std::runtime_error);

    delete c;
}


TEST(CycloidTest, CenterDistance){
    constexpr double accuracy = 1.0e-4;

    auto *c = new Cardioid(1, LEFT);

    for (int i = 1; i < 10; ++i){
        c->SetBaseRadius(i);

        EXPECT_EQ(c->CenterDistance(0), 0);
        EXPECT_NEAR(c->CenterDistance(std::numbers::pi/4), 0.585786 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(1), 0.919395 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(std::numbers::pi/2), 2 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(2), 2.83229 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(std::numbers::pi), 4 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(4.5), 2.42159 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(3 * std::numbers::pi / 2), 2 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(6), 0.0796594 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(2 * std::numbers::pi), 0, accuracy);
        EXPECT_NEAR(c->CenterDistance(2 * std::numbers::pi + 1), 0.919395 * i, accuracy);
    }

    c->SetDirection(RIGHT);

    for (int i = 1; i < 10; ++i){
        c->SetBaseRadius(i);

        EXPECT_NEAR(c->CenterDistance(0), 4 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(std::numbers::pi/4), 3.41421 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(1), 3.0806 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(std::numbers::pi/2), 2 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(2), 1.16771 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(std::numbers::pi), 0, accuracy);
        EXPECT_NEAR(c->CenterDistance(4.5), 1.57841 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(3 * std::numbers::pi / 2), 2 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(6), 3.92034 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(2 * std::numbers::pi), 4 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(2 * std::numbers::pi + 1), 3.0806 * i, accuracy);
    }


    c->SetDirection(UP);

    for (int i = 1; i < 10; ++i){
        c->SetBaseRadius(i);
        EXPECT_NEAR(c->CenterDistance(0), 2 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(std::numbers::pi/4), 3.41421 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(1), 3.68294 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(std::numbers::pi/2), 4 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(2), 3.81859 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(std::numbers::pi), 2 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(4.5), 0.0449398 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(3 * std::numbers::pi / 2), 0 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(6), 1.44117 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(2 * std::numbers::pi), 2 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(2 * std::numbers::pi + 1), 3.68294 * i, accuracy);
    }


    c->SetDirection(DOWN);

    for (int i = 1; i < 10; ++i){
        c->SetBaseRadius(i);

        EXPECT_NEAR(c->CenterDistance(0), 2 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(std::numbers::pi/4), 0.585786 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(1), 0.317058 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(std::numbers::pi/2), 0 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(2), 0.181405 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(std::numbers::pi), 2 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(4.5), 3.95506 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(3 * std::numbers::pi / 2), 4 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(6), 2.55883 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(2 * std::numbers::pi), 2 * i, accuracy);
        EXPECT_NEAR(c->CenterDistance(2 * std::numbers::pi + 1), 0.317058* i,  accuracy);
    }

    delete c;
}

TEST(CycloidTest, MaxAxisDistanceCoords){
    constexpr double accuracy = 1.0e-30;

    auto *c = new Cardioid(1, LEFT);

    CoordinatePair coords = c->MaxAxisDistanceCoords();

    EXPECT_NEAR(coords.x1, -3.0/2, accuracy);
    EXPECT_NEAR(coords.x2, -3.0/2, accuracy);
    EXPECT_NEAR(coords.y1, 3 * sqrt(3) / 2, accuracy);
    EXPECT_NEAR(coords.y2, -3 * sqrt(3) / 2, accuracy);

    c->SetBaseRadius(2);
    coords = c->MaxAxisDistanceCoords();

    EXPECT_NEAR(coords.x1, -3, accuracy);
    EXPECT_NEAR(coords.x2, -3, accuracy);
    EXPECT_NEAR(coords.y1, 3 * sqrt(3), accuracy);
    EXPECT_NEAR(coords.y2, -3 * sqrt(3), accuracy);


    c->SetBaseRadius(1);
    c->SetDirection(RIGHT);
    coords = c->MaxAxisDistanceCoords();

    EXPECT_NEAR(coords.x1, 3.0/2, accuracy);
    EXPECT_NEAR(coords.x2, 3.0/2, accuracy);
    EXPECT_NEAR(coords.y1, 3 * sqrt(3) / 2, accuracy);
    EXPECT_NEAR(coords.y2, -3 * sqrt(3) / 2, accuracy);

    c->SetBaseRadius(2);
    coords = c->MaxAxisDistanceCoords();

    EXPECT_NEAR(coords.x1, 3, accuracy);
    EXPECT_NEAR(coords.x2, 3, accuracy);
    EXPECT_NEAR(coords.y1, 3 * sqrt(3), accuracy);
    EXPECT_NEAR(coords.y2, -3 * sqrt(3), accuracy);


    c->SetBaseRadius(1);
    c->SetDirection(UP);
    coords = c->MaxAxisDistanceCoords();

    EXPECT_NEAR(coords.x1, 3 * sqrt(3) / 2, accuracy);
    EXPECT_NEAR(coords.x2, -3 * sqrt(3) / 2, accuracy);
    EXPECT_NEAR(coords.y1, 3.0/2, accuracy);
    EXPECT_NEAR(coords.y2, 3.0/2, accuracy);

    c->SetBaseRadius(2);
    coords = c->MaxAxisDistanceCoords();

    EXPECT_NEAR(coords.x1, 3 * sqrt(3), accuracy);
    EXPECT_NEAR(coords.x2, -3 * sqrt(3), accuracy);
    EXPECT_NEAR(coords.y1, 3, accuracy);
    EXPECT_NEAR(coords.y2, 3, accuracy);


    c->SetBaseRadius(1);
    c->SetDirection(DOWN);
    coords = c->MaxAxisDistanceCoords();

    EXPECT_NEAR(coords.x1, 3 * sqrt(3) / 2, accuracy);
    EXPECT_NEAR(coords.x2, -3 * sqrt(3) / 2, accuracy);
    EXPECT_NEAR(coords.y1, -3.0/2, accuracy);
    EXPECT_NEAR(coords.y2, -3.0/2, accuracy);

    c->SetBaseRadius(2);
    coords = c->MaxAxisDistanceCoords();

    EXPECT_NEAR(coords.x1, 3 * sqrt(3), accuracy);
    EXPECT_NEAR(coords.x2, -3 * sqrt(3), accuracy);
    EXPECT_NEAR(coords.y1, -3, accuracy);
    EXPECT_NEAR(coords.y2, -3, accuracy);

    delete c;
}

TEST(CycloidTest, CurvatureOfMainPoints){
    constexpr double accuracy = 1.0e-30;

    auto *c = new Cardioid();
    MainPointsCurvature curvature = c->CurvatureOfMainPoints();

    for (int direction = LEFT; direction <= DOWN; ++direction){
        c->SetDirection(static_cast<DirectionEnum>(direction));
        for (int i = 0; i < 10; ++i){
            c->SetBaseRadius(i);
            curvature = c->CurvatureOfMainPoints();

            EXPECT_NEAR(curvature.CuspCurvature, 0, accuracy);
            EXPECT_NEAR(curvature.TopCurvature, 8.0 * i / 3, accuracy);
        }
    }

    delete c;
}

TEST(CycloidTest, Square){
    constexpr double accuracy = 1.0e-30;

    auto *c = new Cardioid();

    for (int direction = LEFT; direction <= DOWN; ++direction){
        c->SetDirection(static_cast<DirectionEnum>(direction));
        for (int i = 0; i < 10; ++i){
            c->SetBaseRadius(i);

            EXPECT_NEAR(c->Square(), 6 * std::numbers::pi * i * i, accuracy);
        }
    }

    delete c;
}

TEST(CycloidTest, ArcLength){
    constexpr double accuracy = 1.0e-4;

    auto *c = new Cardioid(1, LEFT);

    for (int i = 0; i < 10; ++i){
        c->SetBaseRadius(i);

        EXPECT_NEAR(c->ArcLength(0), 0 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(std::numbers::pi/4), 0.608964 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(1), 0.97934 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(std::numbers::pi/2), 2.34315 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(2), 3.67758 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(std::numbers::pi), 8 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(4.5), 13.02539 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(3 * std::numbers::pi / 2), 13.65685 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(6), 15.91994 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(2 * std::numbers::pi), 16 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(2 * std::numbers::pi + 1), 16.97934 * i, accuracy);
    }

    for (int i = 0; i < 10; ++i){
        c->SetBaseRadius(i);

        EXPECT_NEAR(c->ArcLength(0), 0 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-std::numbers::pi/4), 0.608964 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-1), 0.97934 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-std::numbers::pi/2), 2.34315 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-2), 3.67758 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-std::numbers::pi), 8 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-4.5), 13.02539 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-3 * std::numbers::pi / 2), 13.65685 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-6), 15.91994 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-2 * std::numbers::pi), 16 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-2 * std::numbers::pi - 1), 16.97934 * i, accuracy);
    }



    c->SetDirection(RIGHT);

    for (int i = 0; i < 10; ++i){
        c->SetBaseRadius(i);

        EXPECT_NEAR(c->ArcLength(0), 0 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(std::numbers::pi/4), 3.06147 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(1), 3.8354 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(std::numbers::pi/2), 5.65685 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(2), 6.73177 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(std::numbers::pi), 8 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(4.5), 9.77541 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(3 * std::numbers::pi / 2), 10.34315 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(6), 14.87104 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(2 * std::numbers::pi), 16 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(2 * std::numbers::pi + 1), 19.8354 * i, accuracy);
    }

    for (int i = 0; i < 10; ++i){
        c->SetBaseRadius(i);

        EXPECT_NEAR(c->ArcLength(0), 0 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-std::numbers::pi/4), 3.06147 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-1), 3.8354 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-std::numbers::pi/2), 5.65685 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-2), 6.73177 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-std::numbers::pi), 8 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-4.5), 9.77541 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-3 * std::numbers::pi / 2), 10.34315 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-6), 14.87104 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-2 * std::numbers::pi), 16 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-2 * std::numbers::pi - 1), 19.8354 * i, accuracy);
    }


    c->SetDirection(UP);

    for (int i = 0; i < 10; ++i){
        c->SetBaseRadius(i);

        EXPECT_NEAR(c->ArcLength(0), 0 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(std::numbers::pi/4), 2.59539 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(1), 3.40454 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(std::numbers::pi/2), 5.65685 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(2), 7.36052 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(std::numbers::pi), 11.31371 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(4.5), 13.61179 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(3 * std::numbers::pi / 2), 13.65685 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(6), 15.25831 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(2 * std::numbers::pi), 16 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(2 * std::numbers::pi + 1), 19.40454 * i, accuracy);
    }

    for (int i = 0; i < 10; ++i){
        c->SetBaseRadius(i);

        EXPECT_NEAR(c->ArcLength(0), 0 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-std::numbers::pi/4), 1.73418 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-1), 2.01954 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-std::numbers::pi/2), 2.34315 * i, accuracy); //
        EXPECT_NEAR(c->ArcLength(-2), 2.52666 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-std::numbers::pi), 4.6863 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-4.5), 9.49519 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-3 * std::numbers::pi / 2), 10.34315 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-6), 15.1451 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-2 * std::numbers::pi), 16 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-2 * std::numbers::pi - 1), 18.01954 * i, accuracy);
    }


    c->SetDirection(DOWN);

    for (int i = 0; i < 10; ++i){
        c->SetBaseRadius(i);

        EXPECT_NEAR(c->ArcLength(0), 0 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(std::numbers::pi/4), 1.73419 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(1), 2.01955 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(std::numbers::pi/2), 2.34315 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(2), 2.52666 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(std::numbers::pi), 4.6863 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(4.5), 9.49519 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(3 * std::numbers::pi / 2), 10.34315 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(6), 15.1451 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(2 * std::numbers::pi), 16 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(2 * std::numbers::pi + 1), 18.01955 * i, accuracy);
    }

    for (int i = 0; i < 10; ++i){
        c->SetBaseRadius(i);

        EXPECT_NEAR(c->ArcLength(0), 0 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-std::numbers::pi/4), 2.59539 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-1), 3.40454 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-std::numbers::pi/2), 5.65685 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-2), 7.36052 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-std::numbers::pi), 11.31371 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-4.5), 13.61179 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-3 * std::numbers::pi / 2), 13.65685 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-6), 15.25831 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-2 * std::numbers::pi), 16 * i, accuracy);
        EXPECT_NEAR(c->ArcLength(-2 * std::numbers::pi - 1), 19.40454 * i, accuracy);
    }

    delete c;
}


TEST(CycloidTest, AngleConvert){
    constexpr double accuracy = 1.0e-30;

    RevolutionAngle revolutionAngleExpected{0, 0};
    RevolutionAngle revolutionAngleActual{0, 0};

    revolutionAngleExpected.Angle = std::numbers::pi / 2;
    revolutionAngleExpected.RevolutionCount = 0;

    revolutionAngleActual = ConvertAngle(std::numbers::pi / 2);

    EXPECT_NEAR(revolutionAngleExpected.Angle, revolutionAngleActual.Angle, accuracy);
    EXPECT_EQ(revolutionAngleExpected.RevolutionCount, revolutionAngleActual.RevolutionCount);

    revolutionAngleExpected.Angle = std::numbers::pi / 3;
    revolutionAngleExpected.RevolutionCount = 0;

    revolutionAngleActual = ConvertAngle(std::numbers::pi / 3);

    EXPECT_NEAR(revolutionAngleExpected.Angle, revolutionAngleActual.Angle, accuracy);
    EXPECT_EQ(revolutionAngleExpected.RevolutionCount, revolutionAngleActual.RevolutionCount);

    revolutionAngleExpected.Angle = 2 * std::numbers::pi / 3;
    revolutionAngleExpected.RevolutionCount = 0;

    revolutionAngleActual = ConvertAngle(2 * std::numbers::pi / 3);

    EXPECT_NEAR(revolutionAngleExpected.Angle, revolutionAngleActual.Angle, accuracy);
    EXPECT_EQ(revolutionAngleExpected.RevolutionCount, revolutionAngleActual.RevolutionCount);

    revolutionAngleExpected.Angle = 2 * std::numbers::pi / 3;
    revolutionAngleExpected.RevolutionCount = 0;

    revolutionAngleActual = ConvertAngle(-2 * std::numbers::pi / 3);

    EXPECT_NEAR(revolutionAngleExpected.Angle, revolutionAngleActual.Angle, accuracy);
    EXPECT_EQ(revolutionAngleExpected.RevolutionCount, revolutionAngleActual.RevolutionCount);

    revolutionAngleExpected.Angle = std::numbers::pi;
    revolutionAngleExpected.RevolutionCount = 0;

    revolutionAngleActual = ConvertAngle(std::numbers::pi);

    EXPECT_NEAR(revolutionAngleExpected.Angle, revolutionAngleActual.Angle, accuracy);
    EXPECT_EQ(revolutionAngleExpected.RevolutionCount, revolutionAngleActual.RevolutionCount);

    revolutionAngleExpected.Angle = std::numbers::pi + 1;
    revolutionAngleExpected.RevolutionCount = 0;

    revolutionAngleActual = ConvertAngle(std::numbers::pi + 1);

    EXPECT_NEAR(revolutionAngleExpected.Angle, revolutionAngleActual.Angle, accuracy);
    EXPECT_EQ(revolutionAngleExpected.RevolutionCount, revolutionAngleActual.RevolutionCount);

    revolutionAngleExpected.Angle = 0;
    revolutionAngleExpected.RevolutionCount = 1;

    revolutionAngleActual = ConvertAngle(2 * std::numbers::pi);

    EXPECT_NEAR(revolutionAngleExpected.Angle, revolutionAngleActual.Angle, accuracy);
    EXPECT_EQ(revolutionAngleExpected.RevolutionCount, revolutionAngleActual.RevolutionCount);

    revolutionAngleExpected.Angle = 2;
    revolutionAngleExpected.RevolutionCount = 1;

    revolutionAngleActual = ConvertAngle(2 * std::numbers::pi + 2);

    EXPECT_NEAR(revolutionAngleExpected.Angle, revolutionAngleActual.Angle, accuracy);
    EXPECT_EQ(revolutionAngleExpected.RevolutionCount, revolutionAngleActual.RevolutionCount);

    revolutionAngleExpected.Angle = std::numbers::pi + 3;
    revolutionAngleExpected.RevolutionCount = 2;

    revolutionAngleActual = ConvertAngle(5 * std::numbers::pi + 3);

    EXPECT_NEAR(revolutionAngleExpected.Angle, revolutionAngleActual.Angle, accuracy);
    EXPECT_EQ(revolutionAngleExpected.RevolutionCount, revolutionAngleActual.RevolutionCount);
}